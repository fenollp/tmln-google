all: erl.mk | ensure

erl.mk:
	curl -fsSLo $@ 'https://raw.github.com/fenollp/erl-mk/master/erl.mk' || rm $@

include erl.mk
# Your targets after this line.

Dataset = '../20news-bydate/20news-bydate-train/comp.os.ms-windows.misc'
ebin/tmln.beam: ERLCFLAGS += +warn_missing_spec
all:            ERLCFLAGS += +o3 #+native :actually slower

all: escript
clean:
	rm -rf ebin/*

distclean: clean clean-docs
	$(if $(wildcard deps/ ),   rm -rf deps/)
	$(if $(wildcard ebin/ ),   rm -rf ebin/)
#	$(if $(wildcard erl.mk),   rm erl.mk   )
	$(if $(wildcard ./$(APP)), rm ./$(APP) )
	$(if $(wildcard ./erl_crash.dump), rm ./erl_crash.dump )
.PHONY: distclean

debug: ERLCFLAGS += +export_all +debug_info
debug: all
	erl -pa ebin/ -eval 'c:l($(APP)).'

ensure:
	test -f $(APP) && rm $(APP) || true

test: clean escript
	./$(APP) $(Dataset)

demo: clean escript
	./$(APP) ../20news-bydate/20news-bydate-train/*

bench.%:
	bash -c '[[ -x $(APP) ]] && true || make clean all'
	@bash -c 'hash=$$(git rev-parse HEAD 2>/dev/null) && echo $${hash:0:7}: || true; \
        for i in `seq 1 10`; do \
	              ./$(APP) $(Dataset) | grep $*; \
                  done'

PLT = ~/.dialyzer_plt
dialyze: $(PLT)
	dialyzer -r src/ --src
.PHONY: dialyze

$(PLT):
	bash -c 'dialyzer --build_plt --output_plt $(PLT) --apps erts kernel stdlib mnesia compiler crypto'
