%% -*- coding: utf-8 -*-
%%! -smp enable -sname tmln
%% Copyright © 2014 Pierre Fenoll ‹pierrefenoll@gmail.com›
%% See LICENSE for licensing information.
-module(tmln).

%% tmln: Text Mining & Langage Naturel TP1

-export([ main/1      , search/2   , elastic_search/2
        , fetch/1     , fetch/2
        , analyze/2
        , normalizer/1, processor/2
        , build/1     , built/0
        , save/1      , load/1
        , search/1
        ]).

-export_types([ path/0, text/0
              , processor/0    ]).

-type path() :: string().
-type text() :: binary().

-record(document, { text :: text()
                  , url  :: path() }).

-record(tokenized_document, { words :: [text()]
                            , url   :: path() }).

-opaque document() :: #document{}.
-opaque tokenized_document() :: #tokenized_document{}.

-define(log(T),     io:format("~p\n",[T]),).
-define(log(Str,L), io:format(Str,L),).


%% escript Interface

-spec main([path()]) -> [pid()] | term().

main ([]) -> usage();
main ([Path]) -> main1(Path); %% Generations = [self()]
main (Paths) ->
    MainPid = self(),
    io:format("Indexing using workers:\n"),
    Generations =
        [ begin  %% Spawn server-children
              Pid = spawn_link(fun () ->  %% Could be run on another machine
                                       main1([Path]),
                                       MainPid ! 'im_ready!',
                                       serve(Path)
                               end),
              io:format("\tproc ~p  will be indexing  ~p\n", [Pid, Path]),
              Pid
          end || Path <- Paths],
    io:format("   --\n"),
    WorkersCount = length(Generations),
    wait_for_workers(WorkersCount),
    Line         = string:chars($=, 79),
    io:format("\n\n~s\nAll ~p workers ready.\n~s\n\n", [Line,WorkersCount,Line]),
    elastic_search(Generations, [<<"erlang">>]),
    elastic_search(Generations, [<<"algeria">>]),
    io:format("Done\n"),
    Generations.

-spec elastic_search([pid()], [text()]) -> [path()].
elastic_search (Generations, Items) ->
    {T,R} = timer:tc(?MODULE, search, [Generations,Items]),
    elapsed("e_search: ~s \t ~p\n\t\t\t   -> ~p ~p\n",
            [T, Items, length(R), R]),
    R.

main1 (Path) ->  %% See README.md for a clear version of this.
    {T1,Docs} = timer:tc(?MODULE, fetch, [Path]),
    elapsed("fetch  : ~s\n", [T1]),
    Proc = fun normalizer/1,
    {T2,Tokenizeds} = timer:tc(?MODULE, analyze, [Docs, [Proc]]),
    elapsed("analyze: ~s\n", [T2]),
    {T3,ok} = timer:tc(?MODULE, build, [Tokenizeds]),
    elapsed("build  : ~s \t ~s \t ~p files scanned\n", [T3,mem(),files()]),
%   {T4,ok} = timer:tc(?MODULE, save, ["../tmln.save"]),
%   elapsed("save: ~s\n", [T4]), %% Too slow…
%   ?log(built())
    {T5,R1} = timer:tc(?MODULE, search, [Q1= <<"erlang">>]),
    elapsed("search1: ~s \t search(~p)\n\t\t\t   = ~p ~p\n",
            [T5, Q1, length(R1), R1]),
    Q2 = [<<"love">>, <<"you">>],
    {T6,R2} = timer:tc(?MODULE, search, [Q2]),
    elapsed("search2: ~s \t search(~p)\n\t\t\t\t   = ~p ~p\n",
            [T6, Q2, length(R2), R2]),
    elapsed("~sdone\n", [<<>>]).


%% API

-spec fetch(path()) -> [document()].
fetch (Path) ->
    fetch(Path, true).

-spec fetch(path(), boolean()) -> [document()].
fetch (Path, Recursive) ->
    do_fetch(fun extract_document/1, ls(Path), Recursive, []).


-type processor() :: fun((text()) -> text()).
-spec analyze([document()], [processor()]) -> [tokenized_document()].

analyze (Documents, Processors) ->
    plists:map(  %% Done in parallel
      fun (Document) ->
              Words = tokenize(Document#document.text, <<>>, []),
              #tokenized_document
                  { url   = Document#document.url
                  , words = [apply_processors(Processors,Word)||Word<-Words] }
      end, Documents).


-spec (normalizer/1 :: (text()) -> text()). %% normalizer/1 :: processor()
normalizer (Word) ->
    processor(fun string:to_lower/1, Word).


-type index() :: [{text(), path()}].
-spec build([tokenized_document()]) -> ok.

build (TokenizedDocuments) ->
    %% One index generation per process
    case get(url_count) of
        undefined -> put(url_count, 0);
        _________ -> ok
    end,
    lists:foreach(
      fun (Doc) ->
              Words = Doc#tokenized_document.words,
              Url   = Doc#tokenized_document.url,
              %% Assuming every Url is different and not already in the index,
              %%   add Url to hashmap
              HashedUrl = get(url_count) + 1,
              put(HashedUrl, Url),
              put(Url, HashedUrl),
              put(url_count, HashedUrl),
              lists:foreach(
                fun (Word) ->
                        case get(Word) of
                            undefined -> put(Word, [HashedUrl]);
                            Urls      -> put(Word, [HashedUrl|Urls])
                        end
                end, lists:usort(Words))
      end, TokenizedDocuments).


-spec built() -> index().
built () -> get().

-spec save(file:name()) -> ok | {error, Reason::term()}.
save (Filename) ->
    file:write_file(Filename, io_lib:format("~p.\n",[get()])).

-spec load(file:name()) -> ok | {error, Reason::term()}.
load (Filename) ->
    case file:consult(Filename) of
        {ok, KVs} ->  %% Overwrites previous generation
            lists:foreach( %% Assumes no doubles in list!
              fun ({K,V}) -> put(K,V) end, KVs);
        {error, _Reason}=M -> M
    end.


-spec search(text() | [text()]) -> [path()].

search (Word)
  when is_binary(Word) ->
    case get(Word) of
        undefined -> [];
        Urls      -> [get(Url) || Url <- Urls]
    end;
search (Words)
  when is_list(Words) ->
    Urls = lists:flatmap(fun search/1, Words), % = ++ . map
    lists:usort(Urls -- lists:usort(Urls)).


%% Internals

-spec processor(processor(), text()) -> text().
%% Method to use for TextProcessor (See normalizer)
processor (Fun, Binary) ->
    << <<( begin %% Nice binary syntax eh
               <<E>> = Element,
               <<( Fun(E) )>>
           end )/binary>>
       || <<Element:1/binary>> <= Binary>>.


elapsed (Fmt, L) ->
    Time = 'µs'(hd(L)),
    io:format("~p  "++Fmt, [self(), Time|tl(L)]).

'µs' (Int)  %% Pretty print µs
  when is_integer(Int) ->
    <<Hd, Tl/binary>> = integer_to_binary(Int),
    <<Hd, ('µs'(Tl))/binary, "µs"/utf8>>;
'µs' (<<>>) -> <<>>;
'µs' (<<I, Str/binary>>=Bin)
  when size(Bin) rem 3 =:= 0 ->
    <<$', I, ('µs'(Str))/binary>>;
'µs' (<<I, Str/binary>>) ->
    <<I, ('µs'(Str))/binary>>.

mem () ->
    {_,MemUsed} = process_info(self(), memory),
    Separated = 'µs'(MemUsed),
    MemSize = size(Separated) - size(<<"µs"/utf8>>),
    <<PrettyMem:MemSize/binary, "µs"/utf8>> = Separated,
    <<PrettyMem/binary, "B">>.

files () ->
    get(url_count).


do_fetch (_, [], _, Acc) ->
    Acc;
do_fetch (Fun, [Path|Rest], Recursive, Acc) ->
    case filelib:is_dir(Path) of
        false ->
            case Fun(Path) of
                ignore -> do_fetch(Fun, Rest, Recursive,      Acc );
                Val    -> do_fetch(Fun, Rest, Recursive, [Val|Acc])
            end;
        true  ->
            case Recursive of
                false -> do_fetch(Fun,           Rest, Recursive, Acc);
                true  -> do_fetch(Fun, ls(Path)++Rest, Recursive, Acc)
            end
    end.

ls (Path) ->
    {ok, Listing} = file:list_dir(Path),
    [filename:join(Path, Name) || Name <- Listing].

extract_document (Path) ->
    case prim_file:read_file(Path) of
        {ok, Contents} ->
            #document{url=Path, text=Contents};
        {error, Reason} ->
            ?log("Ignored '~s' because of ~p\n", [Path,Reason])
            ignore
    end.


%% re:run("céeµatß ","\\w+",[global,{capture,all,binary}]) --> lexes
tokenize (<<>>, <<>>, WordAcc) -> WordAcc;
tokenize (<<>>, CharAcc, WordAcc) ->
    tokenize(<<>>, <<>>, [CharAcc|WordAcc]);
tokenize (<<Char, Rest/binary>>, CharAcc, WordAcc) ->
    case is_separator(Char) of
        false ->
            NewCharAcc = <<CharAcc/binary, Char>>,
            tokenize(Rest, NewCharAcc, WordAcc);
        true  ->
            case CharAcc of
                <<>> ->
                    tokenize(Rest, <<>>, WordAcc);
                _    ->
                    tokenize(Rest, <<>>, [CharAcc|WordAcc])
            end
    end.

is_separator (Char) ->
    case Char of
        Lower when ($a =< Lower) and (Lower =< $z) -> false;
        Upper when ($A =< Upper) and (Upper =< $Z) -> false;
        Numbr when ($0 =< Numbr) and (Numbr =< $9) -> false;
        _ -> true
    end.

apply_processors ([], Word) -> Word;
apply_processors ([Processor|Processors], Word) ->
    apply_processors(Processors, Processor(Word)).



wait_for_workers (0) -> done_waiting;
wait_for_workers (N) ->
    receive 'im_ready!' ->
            wait_for_workers(N -1) end.


-spec search([pid()], [text()]) -> [path()].

search (Pids, Query) ->
    MainPid = self(),
    WorkUnit = make_ref(),
    lists:foreach(fun (Pid) ->
                          Pid ! {MainPid, search, Query, WorkUnit}
                  end, Pids),
    gather_results(length(Pids), WorkUnit, []).

gather_results (0, _, Acc) ->
    lists:append(Acc);
gather_results (N, Ref, Acc) ->
    receive
        {Ref, results, Part} ->
            gather_results(N -1, Ref, [Part|Acc]);
        ImpromptuMessage     ->
            io:format("Yo! Got this weird msg: ~p\n", [ImpromptuMessage]),
            gather_results(N, Ref, Acc)
    after 2000 -> %% in ms
            io:format("Search timed out! ~p ~p ~p\n", [N,Ref,Acc]),
            timeout
    end.

serve (Id) ->
    receive
        {From, search, Items, Ref} ->
            LocalResults = lists:flatmap(fun search/1, Items),
            From ! {Ref, results, LocalResults},
            serve(Id);
        {From, stop} ->
            From ! {self(), stopped},
            ok
    end.


usage () ->
    ok = io:setopts([{encoding, unicode}]),
    Arg0 = escript:script_name(),
    io:format("Usage: ~s  ‹path to folder›⁺\n",
              [filename:basename(Arg0)]),
    halt(1).

%% End of Module.
